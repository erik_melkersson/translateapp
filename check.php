<?php
include 'include/common.php';
try {
    $dbh = getDbh();
    $editor = getEditor($dbh);
    htmlHead("Check placeholders, ". settingProjectName(), $editor);
    if (!$editor) {
        echo "<p>Only for editors</p>";
    } else {
        checkData($dbh);
    }
    htmlFoot();
} catch (Exception $exc) {
    logError($exc->getTraceAsString());
}

function checkData($dbh) {
    $stringkeys = getStringKeys($dbh);
    $defaultkeys = getTranslationHash($dbh, 1);
    
    echo '<p>Listing not matching placeholders in translations here. It is up to you to check them.</p>';
    echo '<p><strong>NOTE: Does not check arrays.</strong></p>';    
    $counter = 0;
    
    echo '<ul>';
    foreach (getLanguages($dbh) as $lang) {
        if ($lang->id == 1) {continue;}
        echo '<li><strong>'.$lang->id . '|'.$lang->nameAndCode().'</strong>';
            $translations = getTranslationHash($dbh, $lang->id);
            echo '<ul>';

            foreach ($stringkeys as $stringKey) {
                if (isset($translations[$stringKey->id . '_0'])) {
                    $counter++;
                    $default = $defaultkeys[$stringKey->id . '_0'];
                    $translation = $translations[$stringKey->id . '_0'];
                    
                    $dt = $default->text;
                    $tt = $translation->text;
                    
                    // If there are any %-characters at all
                    if (strpos($tt, '%') !== false || strpos($dt, '%') !== false) {
                        $darr = findSeq($dt);
                        sort($darr);
                        $tarr = findSeq($tt);
                        sort($tarr);
                        if ($darr !== $tarr) {
                            //To make a link to editing it: $translation->id.
                            echo '<li><code>'. json_encode($darr).'</code> '.htmlentities($default->text, ENT_COMPAT | ENT_HTML401, "UTF-8").
                                    '<br><code>'. json_encode($tarr).'</code> '.htmlentities($translation->text, ENT_COMPAT | ENT_HTML401, "UTF-8").
                                    ' <a href="trans-string-edit.php?l='.$lang->id.'&amp;k='.$stringKey->id.'">Edit</a></li>'.
                                    '</li>';
                        }
                    }
                }
            }

            
            echo '</ul>';            
        echo '</li>';
    }
    echo '</ul>';
    echo '<p>Checked '.$counter.' translated strings.</p>';

}

// Find all sequences beginning with %
function findSeq($string) {
    $arr = array();
    
    // https://developer.android.com/reference/java/util/Formatter.html
    // Syntax of the formatting strings
    // %[argument_index$][flags][width][.precision]conversion
    //    number                       bBhHsScCdoxXeEfgGaAtTn - but tT have one extra 
    
    preg_match_all('/%(\d+\$)?[\-\#\+\ 0,\(]*\d*(\.\d+)?([bBhHsScCdoxXeEfgGaAn]|[tT][a-zA-Z])?/', 
            str_replace('%%', '', $string), // Ignore "litteral" %-signs, i.e. double ones
            $arr, PREG_PATTERN_ORDER);
    return $arr[0]; // Return only the full matches
}

 ?>
