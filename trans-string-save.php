<?php 
include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);
$langId = filter_input(INPUT_GET, 'l', FILTER_VALIDATE_INT);
$keyId = filter_input(INPUT_GET, 'k', FILTER_VALIDATE_INT);
$version = filter_input(INPUT_GET, 'v', FILTER_VALIDATE_INT);
$text = trim(filter_input(INPUT_GET, 'text', FILTER_UNSAFE_RAW));
$description = filter_input(INPUT_GET, 'description', FILTER_UNSAFE_RAW);
$stringkey = filter_input(INPUT_GET, 'stringkey', FILTER_UNSAFE_RAW);
$nextTodo = filter_input(INPUT_GET, 'next', FILTER_VALIDATE_BOOLEAN);

$lastTranslation = getTranslation($dbh, $keyId, $langId, $version);

if ($langId == DEFAULT_TRANSLATION_ID) {
    // Check/set translatable status
    $translatable = filter_input(INPUT_GET, 'translatable', FILTER_VALIDATE_BOOLEAN);
    /* @var $stringKey StringKey  */
    $stringKey = getStringKey($dbh, $keyId);
    if ($translatable != $stringKey->translatable) {
        if ($editor->admin) {
            setStringKeyTranslatable($dbh, $keyId, $translatable);
        } else {
            echo '<p>Only admin may toggle translatable</p>';
        }
    }

    // Check if text changed at all
    if (is_null($lastTranslation) || $text != $lastTranslation->text) {
        // check if only a minor update and use same version and only increase revision 
        $minor = filter_input(INPUT_GET, 'minor', FILTER_VALIDATE_BOOLEAN) || !$editor->admin; // Only admin may make major updates
        if ($minor && !is_null($lastTranslation)) {
            addTranslation($dbh, $editor->id, $keyId, $langId, $version, $lastTranslation->revision + 1, $text);
        } else {
            $newVersion = updateLastVersion($dbh, $keyId);
            addTranslation($dbh, $editor->id, $keyId, $langId, $newVersion, 0, $text);
        }
    }
    
    // Check if key was changed
    if ($editor->admin && $stringkey && $stringkey != $stringKey->skey) {
        setStringKeyName($dbh, $keyId, $stringkey);
    }
    // Check if description was changed
    if ($editor->admin && $description && $description != $stringKey->description) {
        setStringKeyDescription($dbh, $keyId, $description);
    }
} else {
    // Translation to another language
    $revision = 0;
    if (!is_null($lastTranslation)) {
        /* @var $lastTranslation Translation  */
        $revision = ($lastTranslation->revision ?? 0) + 1;
    }

    // If the same: ignore update.
    if (is_null($lastTranslation) || $text != ($lastTranslation->text ?? null)) {
        addTranslation($dbh, $editor->id, $keyId, $langId, $version, $revision, $text);
    }
}

if ($nextTodo) {
    // TODO Find a string without translation
    $otherKeyId = findNextTodo($dbh, $langId);
    if ($otherKeyId == null) {
        // Nothing more to translate
        header('Location: lang-edit.php?l='.$langId);
    } else {
        $otherKey = getStringKey($dbh, $otherKeyId);
        // Redirect to it
        if ($otherKey->array) {
            header('Location: trans-array-edit.php?l='.$langId.'&k='.$otherKeyId.'&next=1');
        } else {
            header('Location: trans-string-edit.php?l='.$langId.'&k='.$otherKeyId.'&next=1');
        }
    }
} else {
    header('Location: lang-edit.php?l='.$langId);
}

function findNextTodo($dbh, $langId) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT k.id FROM '.$prefix.'stringkey AS k LEFT JOIN '.$prefix.'translation AS t ON k.id=t.id AND k.lastversion=t.version AND t.lang=? WHERE k.translatable=1 AND k.active=1 AND t.id IS NULL ORDER BY rand() LIMIT 1');
    $stmt->execute(array($langId));
    $row = $stmt->fetch();
    if ($row) { return $row['id']; }
    return null;
}