<?php 
include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);
$langId = filter_input(INPUT_GET, 'l', FILTER_VALIDATE_INT);
$keyId = filter_input(INPUT_GET, 'k', FILTER_VALIDATE_INT);
$version = filter_input(INPUT_GET, 'v', FILTER_VALIDATE_INT);

$translations = getTranslationHash($dbh, $langId);
$defaultTranslations = getTranslationHash($dbh, DEFAULT_TRANSLATION_ID);

$description = filter_input(INPUT_GET, 'description', FILTER_UNSAFE_RAW);
$stringkey = filter_input(INPUT_GET, 'stringkey', FILTER_UNSAFE_RAW);

if ($langId == DEFAULT_TRANSLATION_ID) {
    
    // Check/set translatable status
    $translatable = filter_input(INPUT_GET, 'translatable', FILTER_VALIDATE_BOOLEAN);
    $stringKey = getStringKey($dbh, $keyId);
    if ($translatable != $stringKey->translatable) {
        if ($editor && $editor->admin) {
            setStringKeyTranslatable($dbh, $keyId, $translatable);
        } else {
            echo '<p>Only admin may toggle translatable</p>';
        }
    }

    // If not invalidating translations
    $minor = filter_input(INPUT_GET, 'minor', FILTER_VALIDATE_BOOLEAN);// || !$editor->admin; // Only admin may make major updates

    // Get strings - NOTE: empty ones are removed
    $maxSorder = getTranslationMaxSorder($dbh, $keyId);
    $text = array();
    for ($sorder = 0; $sorder <= $maxSorder; $sorder++) {
        $addText = filter_input(INPUT_GET, 'text'.$sorder, FILTER_UNSAFE_RAW);
        if ($addText != '')  {$text[] = $addText;}
    }
    // Get extra strings if there are any
    for (; $sorder <= $maxSorder + 5; $sorder++) {
        $newText = filter_input(INPUT_GET, 'text'.$sorder, FILTER_UNSAFE_RAW);
        if (isset($newText) && $newText != '') {$text[] = $newText;}
    }
    
    // Do they differ from current version?
    $equal = true;
    for($sorder = 0; $sorder < sizeof($text); $sorder++) {
        if (!isset($defaultTranslations[$keyId.'_'.$sorder]) || $defaultTranslations[$keyId.'_'.$sorder]->text != $text[$sorder]) {
            $equal = false; break;
        }
    }
    
    // If the amount differ, it is never a minor change
    if ($maxSorder + 1 != sizeof($text)) {
        $equal = FALSE;
        $minor = FALSE;
    }
    
    // Update if different
    if (!$equal) {
        if ($minor) { // Minor update, dont invalidate translations
            $lastTranslation = getTranslation($dbh, $keyId, $langId, $version);
            $newVersion = $version;
            $newRevision = $lastTranslation->revision + 1;
        } else {
            $newVersion = updateLastVersion($dbh, $keyId);
            $newRevision = 0;
        }
        
        for($sorder = 0; $sorder < sizeof($text); $sorder++) {
            addArrayTranslation($dbh, $editor->id, $keyId, $sorder, $langId, $newVersion, $newRevision, $text[$sorder]);
        }
    }

    // Check if key was changed
    if ($editor->admin && $stringkey && $stringkey != $stringKey->skey) {
        setStringKeyName($dbh, $keyId, $stringkey);
    }
    // Check if description was changed
    if ($editor->admin && $description && $description != $stringKey->description) {
        setStringKeyDescription($dbh, $keyId, $description);
    }

} else {
    // Get strings - exactly as entered
    $maxSorder = getTranslationMaxSorder($dbh, $keyId);
    $text = array();
    for ($sorder = 0; $sorder <= $maxSorder; $sorder++) {
        $addText = trim(filter_input(INPUT_GET, 'text'.$sorder, FILTER_UNSAFE_RAW));
        if ($addText != '')  {$text[$sorder] = $addText;}
    }
    
    // Do they differ from current version?
    $equal = true;
    for($sorder = 0; $sorder < sizeof($text); $sorder++) {
        if (!isset($translations[$keyId.'_'.$sorder]) || $translations[$keyId.'_'.$sorder]->text != $text[$sorder]) {
            $equal = false; break;
        }
    }
    
    if (!$equal) {
        $lastTranslation = getTranslation($dbh, $keyId, $langId, $version);
        if ($lastTranslation == null) {
            $newRevision = 0;
        } else {
            $newRevision = $lastTranslation->revision + 1;
        }
        
        for($sorder = 0; $sorder <= $maxSorder; $sorder++) {
            addArrayTranslation($dbh, $editor->id, $keyId, $sorder, $langId, $version, $newRevision, $text[$sorder]);
        }
    }
}

header('Location: lang-edit.php?l='.$langId);