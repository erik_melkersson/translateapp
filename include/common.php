<?php
if (!@include('settings.php')) {
    htmlHead('Setup translate before use');
    echo '<p>Have a look at <a href="https://bitbucket.org/erik_melkersson/translateapp">Setup instructions</a> on what to do first.</p>';
    htmlFoot();
    exit();
}
@include('local.php'); // If any locally defined stuff, like login handling

function getDbTablePrefix() {
    if (function_exists('settingDbTablesPrefix')) { 
        // Avoid sql-injection-possibilites a bit at least, only letters, numbers and underscore.
        return preg_replace("/[^a-zA-Z0-9_]+/", "", settingDbTablesPrefix());
    }
    return '';
}

const DEFAULT_TRANSLATION_ID = 1;
const ADMIN_USER_ID = 1;

function htmlHead($name, $editor = false) {
    if (function_exists('settingTopLogoUrl')) { $topLogoUrl = settingTopLogoUrl(); }
    if (is_null($topLogoUrl)) { $topLogoUrl = 'translate-logo.png'; }
    header('Content-Type: text/html; charset=utf-8');
    $useGoogleSignInId = function_exists('settingGoogleSignInClientId') ? settingGoogleSignInClientId() : false;
    echo '<!DOCTYPE html>
<html lang = "en-US">
 <head>
 <meta charset = "UTF-8">
 <meta name="robots" content="noindex, nofollow">
 <title>'.$name.'</title>
 <link rel="stylesheet" type="text/css" href="common.css">
 <link rel="stylesheet" type="text/css" href="style/style.css">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 ';
    if ($useGoogleSignInId) {
        // https://developers.google.com/identity/gsi/web/guides/display-google-one-tap
        echo '<script src="https://accounts.google.com/gsi/client" async defer></script>';
    }
    echo '</head><body>';
    echo '<img src="'.$topLogoUrl.'" class="top-logo">';
    if ($useGoogleSignInId) {
        if (!$editor) {
            // Show sign in button
              echo '<div id="g_id_onload"
     data-client_id="'.$useGoogleSignInId.'"
     data-context="signin"
     data-ux_mode="popup"
     data-login_uri="'.htmlentities(str_contains($_SERVER['REQUEST_URI'], '.php') ? dirname($_SERVER['REQUEST_URI']) : $_SERVER['REQUEST_URI']).'/signin.php"
     data-returnurl="'.htmlentities($_SERVER['REQUEST_URI']).'"
     data-auto_prompt="false">
</div>
<div class="g_id_signin"
     data-type="standard"
     data-shape="rectangular"
     data-theme="outline"
     data-text="signin_with"
     data-size="large"
     data-logo_alignment="left">
</div>';
            /*
            echo '<div id="g_id_onload"
     data-client_id="'.$useGoogleSignInId.'"
     data-login_uri="'.htmlentities(str_contains($_SERVER['REQUEST_URI'], '.php') ? dirname($_SERVER['REQUEST_URI']) : $_SERVER['REQUEST_URI']).'/signin.php"
     data-returnurl="'.htmlentities($_SERVER['REQUEST_URI']).'"></div>';
             */
        } else {
            echo '<p>Signed in as <b>'.$editor->username.'</b>';
        }
    }
    
    echo '<h1>'.$name.'</h1>';
}

function htmlFoot() {
    echo ' </body>
</html>';
}

function htmlBackLink($langId = 0) {
    echo '<p><a href="';
    if ($langId > 0) {
        echo 'lang-edit.php?l='.$langId;
    } else {
        echo 'index.php';
    }
    echo '">&lt;= Back</a></p>';
}

function logError($msg) {
    ob_start();
    debug_backtrace();
    $err = ob_get_clean();
    ob_end_clean();

    error_log($err);
  
    $date = new DateTime();
    $dateString = $date->format('y-m-d h:i:s');
    error_log($dateString . ' SQL ERROR:'.$msg."\n".$err."\n", 3, '/tmp/translate-error.log');
}

function getDbh() {
    // Get a database connection
    try {
        $dbArr = settingDbPdoParams();
        $dbh = new PDO($dbArr[0], $dbArr[1], $dbArr[2]); // ($dsn, $dbuser, $dbpassword);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // Fail instantly on errors
        return $dbh;
    } catch (PDOException $e) {
        logError('Unable to get database connection:' . $e->getMessage());
        http_response_code(500);
        echo '<html><body>Unable to connect to database</html></body>';
    }
    
}

function initDb($dbh) {
    $wasSetUp = FALSE;
    $tablePrefix = getDbTablePrefix();
    if (!tableExists($dbh, $tablePrefix . 'editor')) {
        $dbh->exec('CREATE TABLE '.$tablePrefix.'editor ('.
                'id INT AUTO_INCREMENT PRIMARY KEY,'.
                'account VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,'.
                'created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,'.
                'googleid VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,'.
                'email VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,'.
                'session VARCHAR(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,'.
                'UNIQUE INDEX (account)'.
                ')');
        $stmt = $dbh->prepare('INSERT INTO '.$tablePrefix.'editor (id, account) VALUES (?,?)');
        $stmt->execute(array(ADMIN_USER_ID, ''));
        $wasSetUp = TRUE;
    }
    if (!tableHasField($dbh, $tablePrefix.'editor', 'googleid')) {
        $dbh->exec('ALTER TABLE '.$tablePrefix.'editor ADD COLUMN googleid VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL');
        $dbh->exec('ALTER TABLE '.$tablePrefix.'editor ADD COLUMN email VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL');
        $dbh->exec('ALTER TABLE '.$tablePrefix.'editor ADD COLUMN session VARCHAR(36) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL');
        $wasSetUp = TRUE;
    }
    if (!tableExists($dbh, $tablePrefix.'language')) {
        $dbh->exec('CREATE TABLE '.$tablePrefix.'language ('
                . 'id INT AUTO_INCREMENT PRIMARY KEY,'
                . 'code VARCHAR(7) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,'
                . 'name VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,'
                . 'created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,'
                . 'editor INT NOT NULL,'
                . 'UNIQUE INDEX (code),'
                . 'INDEX (editor),'
                . 'FOREIGN KEY (editor) REFERENCES '.$tablePrefix.'editor(id)'
                . ')');
        $stmt = $dbh->prepare('INSERT INTO '.$tablePrefix.'language (editor, id, code, name) VALUES (?,?,?,?)');
        $stmt->execute(array(ADMIN_USER_ID, DEFAULT_TRANSLATION_ID, 'DEFAULT', 'DEFAULT')); // default language
        $wasSetUp = TRUE;
    }
    if (!tableExists($dbh, $tablePrefix.'stringkey')) {
        $dbh->exec('CREATE TABLE '.$tablePrefix.'stringkey ('.
                'id INT AUTO_INCREMENT PRIMARY KEY,'.
                'translatable BOOLEAN NOT NULL,'.
                'array BOOLEAN NOT NULL,'.
                'skey VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,'.
                'description VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,'.
                'active BOOLEAN DEFAULT TRUE,'.
                'lastversion INT NOT NULL,'.
                'UNIQUE INDEX (skey)'.
                ')');
        $wasSetUp = TRUE;
    }
    if (!tableHasField($dbh, $tablePrefix.'stringkey', 'description')) {
        $dbh->exec('ALTER TABLE '.$tablePrefix.'stringkey ADD COLUMN description VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci');
        $wasSetUp = TRUE;
    }
    if (!tableHasField($dbh, $tablePrefix.'stringkey', 'active')) {
        $dbh->exec('ALTER TABLE '.$tablePrefix.'stringkey ADD COLUMN active BOOLEAN DEFAULT TRUE');
        $wasSetUp = TRUE;
    }
    if (!tableExists($dbh, $tablePrefix.'translation')) {
        $dbh->exec('CREATE TABLE '.$tablePrefix.'translation ('
                . 'id INT NOT NULL,'
                . 'sorder INT NOT NULL,'
                . 'lang INT NOT NULL,'
                . 'version INT NOT NULL,' // requires new translation
                . 'revision INT NOT NULL,' // does NOT require new translation, also used for updates on translated strings on same version
                . 'created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,'
                . 'editor INT NOT NULL,'
                . 'text VARCHAR(16000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,'
                . 'UNIQUE INDEX (id,sorder,lang,version,revision),'
                . 'INDEX (editor),'
                . 'FOREIGN KEY (id) REFERENCES '.$tablePrefix.'stringkey(id) ON DELETE CASCADE,'
                . 'FOREIGN KEY (lang) REFERENCES '.$tablePrefix.'language(id),'
                . 'FOREIGN KEY (editor) REFERENCES '.$tablePrefix.'editor(id)'
                . ');');     
        $wasSetUp = TRUE;
    }
    return $wasSetUp;
}

// Note not checking that $table is safe
function tableExists($dbh, $table) {
    $result = $dbh->query("SHOW TABLES LIKE '$table'");
    return $result !== false && $result->rowCount() > 0;
}
// Note not checking that $table/$field is safe
function tableHasField($dbh, $table, $field) {
    $result = $dbh->query("SHOW COLUMNS FROM $table LIKE '$field'");
    return $result !== false && $result->rowCount() > 0;    
}

function getSessionCookieName() {
    if (function_exists('settingCookieName')) {
        return settingCookieName();
    }
    return 'TRANSLATEAPP';
}

/////////////////
// Editor

class User {
    public $username;
    public $id;
    public $googleid;
    public $admin;
    public $tcount; // used for translations count and last translations
    public $tcreated; // used for last translation made
    public $lname; // language name, used for last translation made
}

function getEditor($dbh) {
    if (function_exists('local_getEditor')) {
        return local_getEditor($dbh);
    }
    if (isset($_COOKIE[getSessionCookieName()])) {
        $user = getEditorBySession($dbh, $_COOKIE[getSessionCookieName()]);
        if ($user) { $user->admin = false; } // default to false and make sure it is set
        if ($user !== false && function_exists('settingIsAdminGoogleId')) {
            $tmpA = settingIsAdminGoogleId($user->googleid);
            $user->admin = $tmpA;
        }
        return $user;
    } else {
        $username = $_SERVER['PHP_AUTH_USER'] ?? null;
        if (!$username) { $username = $_SERVER['REMOTE_USER'] ?? null;}
        if (!$username) { return null; }
        $user = new User();
        $user->id = getEditorId($dbh, $username);
        $user->username = $username;
        $user->admin = settingIsAdmin($username);
        return $user;
    }
}

function deleteSession($dbh, $session) {
    $stmt = $dbh->prepare('UPDATE '.getDbTablePrefix().'editor SET session=NULL WHERE session=?');
    $stmt->execute(array($session));
}

function getEditorBySession($dbh, $session) {
    $stmt = $dbh->prepare('SELECT id,googleid,account AS username FROM '.getDbTablePrefix().'editor WHERE session=?');
    $stmt->execute(array($session));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
    return $stmt->fetch();
}

// Note: Assumes the use is logged in as $account
// Return id(>0) or 0 if the user is blocked
function getEditorId($dbh, $account) {
    // Find existing one
    $stmt = $dbh->prepare('SELECT id FROM '.getDbTablePrefix().'editor WHERE account = ?'); //Future: check if blocked
    $stmt->execute(array($account));
    $id = $stmt->fetchColumn();
    if ($id) { return $id; }
    // Create it
    $stmtI = $dbh->prepare('INSERT INTO '.getDbTablePrefix().'editor (account) VALUES (?)');
    $stmtI->execute(array($account));
    return $dbh->lastInsertId();
}

// Note: Assumes the use is logged in as $account
// Return id(>0) or 0 if the user is blocked
function getEditorSessionByGoogleId($dbh, $googleId, $name, $email, $autoCreate) {
    // Generate a random string
    $session = sprintf('%04x%04x%04x%04x%04x%04x%04x%04x%04x', 
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), 
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), 
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
    // Find existing one
    $stmt = $dbh->prepare('UPDATE '.getDbTablePrefix().'editor SET session = ? WHERE googleId = ?');
    $stmt->execute(array($session, $googleId));
    if ($stmt->rowCount() > 0) { return $session; }
    if (function_exists('settingGetAccountNameFromGoogleId')) {
        $name = settingGetAccountNameFromGoogleId($googleId);
        if (!$name) { return null; } // User was not ok
        try {
            createAccountAndSetSession($dbh, $googleId, $name, $email, $session);
        } catch (Exception $e) {
            echo $name . ' is already occupied. Please contact administrator.';
            exit();
        }
        return $session;
    } else if ($autoCreate) { 
        createAccountAndSetSession($dbh, $googleId, $name, $email, $session);
        return $session;
    }
    return null; 
}

function createAccountAndSetSession($dbh, $googleId, $name, $email, $session) {
    // Create it
    $stmtI = $dbh->prepare('INSERT INTO '.getDbTablePrefix().'editor (account,googleid,email,session) VALUES (?,?,?,?)');
    $stmtI->execute(array($name,$googleId,$email,$session));
}


function getUsersWithTranslationCount($dbh) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT e.id,e.username,GROUP_CONCAT(CONCAT(tcount, " ", l.name) SEPARATOR ", ") AS tcount FROM (SELECT e.id,e.account AS username,count(t.id) as tcount,t.lang FROM '.$prefix.'editor AS e LEFT JOIN '.$prefix.'translation AS t ON e.id=t.editor GROUP BY e.id,t.lang HAVING tcount > 0) AS e LEFT JOIN '.$prefix.'language AS l ON e.lang=l.id GROUP BY e.id');
    //$stmt = $dbh->prepare('SELECT e.id,e.account AS username,count(t.id) as tcount FROM '.$prefix.'editor AS e LEFT JOIN '.$prefix.'translation AS t ON e.id=t.editor GROUP BY e.id HAVING tcount > 0');
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS, 'User');
}

// Return a list of last edits by a user
// editor, datetime
function getUsersWithLastEdits($dbh) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT e.id,e.account AS username,l.name AS lname,MAX(t.edited) as tcreated,count(t.id) AS tcount FROM (SELECT DISTINCT id,lang,editor,DATE(created) AS edited FROM '.$prefix.'translation ORDER BY created DESC LIMIT 500) AS t LEFT JOIN '.$prefix.'editor AS e ON t.editor=e.id LEFT JOIN '.$prefix.'language AS l ON t.lang=l.id GROUP BY t.editor,t.lang ORDER BY tcreated DESC');  
//    $stmt = $dbh->prepare('SELECT e.id,e.account AS username,max(t.created) as tcreated FROM '.$prefix.'editor AS e JOIN '.$prefix.'translation AS t ON e.id=t.editor GROUP BY e.id ORDER BY tcreated DESC LIMIT 5');
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS, 'User');
}

/////////////////
// Language 

class Language {
    public $id;
    public $code;
    public $name;
    public function htmlLink() {
        return sprintf('<a href="lang-edit.php?l=%s">%s (%s)</a>',$this->id,$this->name,$this->code);
    }
    public function nameAndCode() {
        return sprintf('%s (%s)',$this->name,$this->code);
    }
}

function getLanguages($dbh) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT * FROM '.$prefix.'language ORDER BY id');
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Language');
}

function getLanguage($dbh, $id) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT * FROM '.$prefix.'language WHERE id = ?');
    $stmt->execute(array($id));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Language');
    return $stmt->fetch();
}

function getLanguageByCode($dbh, $code) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT * FROM '.$prefix.'language WHERE code = ?');
    $stmt->execute(array($code));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Language');
    return $stmt->fetch();
}

function addLanguage($dbh, $editorId, $code, $name) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('INSERT INTO '.$prefix.'language (editor, code, name) VALUES (?,?,?)');
    $stmt->execute(array($editorId, $code, $name));
    return $dbh->lastInsertId();
}

// Note: This SHALL fail if there are translations left
function deleteLanguage($dbh, $id) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('DELETE FROM '.$prefix.'language WHERE id = ?');
    $stmt->execute(array($id));
    return $dbh->lastInsertId();
}

///////////////
// key

class StringKey {
    public $id;
    public $skey;
    public $translatable;
    public $array;
    public $lastversion;
    public $description;
}

function addStringKey($dbh, $stringkey, $translatable, $array, $description) {
    $translatable = $translatable ? 1 : 0; // Set useful type for db
    $array = $array ? 1 : 0; // Set useful type for db
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('INSERT INTO '.$prefix.'stringkey (skey, translatable, array, lastversion, description) VALUES (?,?,?,0,?)');
    $stmt->execute(array($stringkey, $translatable, $array, $description));
    return $dbh->lastInsertId();
}

function getStringKeys($dbh) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT * FROM '.$prefix.'stringkey WHERE active=TRUE ORDER BY skey');
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_CLASS, 'StringKey');
}

function getStringKey($dbh, $keyId) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT * FROM '.$prefix.'stringkey WHERE id=?');
    $stmt->execute(array($keyId));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'StringKey');
    return $stmt->fetch();
}

function updateLastVersion($dbh, $keyId) {
    $stringKey = getStringKey($dbh, $keyId);
    /* @var $stringKey StringKey */
    $newVersion = $stringKey->lastversion + 1;
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('UPDATE '.$prefix.'stringkey SET lastversion = ? WHERE id=?');
    $stmt->execute(array($newVersion, $keyId));
    return $newVersion;
}

// Gives a hash based on key
function getStringKeyHash($dbh) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT * FROM '.$prefix.'stringkey');
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'StringKey');
    $hash = array();
    while ($stringKey = $stmt->fetch()) {
        $hash[$stringKey->skey] = $stringKey;
    }
    return $hash;
}

function setStringKeyTranslatable($dbh, $keyId, $translatable) {
    $prefix = getDbTablePrefix();
    $translatable = $translatable ? 1 : 0; // Set useful type for db
    $stmt = $dbh->prepare('UPDATE '.$prefix.'stringkey SET translatable = ? WHERE id=?');
    $stmt->execute(array($translatable, $keyId));
}

function setStringKeyArray($dbh, $keyId, $translatable) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('UPDATE '.$prefix.'stringkey SET array = ? WHERE id=?');
    $stmt->execute(array($translatable, $keyId));
}

function setStringKeyDescription($dbh, $keyId, $description) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('UPDATE '.$prefix.'stringkey SET description = ? WHERE id=?');
    $stmt->execute(array($description, $keyId));
}

function setStringKeyName($dbh, $keyId, $stringkey) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('UPDATE '.$prefix.'stringkey SET skey = ? WHERE id=?');
    $stmt->execute(array($stringkey, $keyId));
}

function inactivateStringKey($dbh, $keyId) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('UPDATE '.$prefix.'stringkey SET active = FALSE WHERE id=?');
    $stmt->execute(array($keyId));
}

////////////////
// Translations

class Translation {
    public $id;
    public $sorder;
    public $lang;
    public $langname;
    public $text;
    public $version;
    public $revision;
    public $created;
    public $editor;
    public $editorname;
}

// Note: Return a hash based on id_sorder and sorder is 0 on strings and can have a value in arrays
// Only return active versions
function getTranslationHash($dbh, $langId) {
    $prefix = getDbTablePrefix();
    $hash = array();
    $stmt = $dbh->prepare('SELECT t.* FROM '.$prefix.'stringkey AS k LEFT JOIN '.$prefix.'translation AS t ON k.id=t.id AND k.lastversion=t.version WHERE lang = ? AND k.active=TRUE');
    //$stmt = $dbh->prepare('SELECT * FROM '.$prefix.'translation WHERE lang = ?');
    $stmt->execute(array($langId));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Translation');
    while ($translation = $stmt->fetch()) {
        /* @var $translation Translation  */
        $hashkey = $translation->id .'_'. $translation->sorder;
        if (isset($hash[$hashkey])) {
            // Only get the last version/revision - TODO Do this in sql instead
            if (/*$translation->version > $hash[$hashkey]->version || 
                    $translation->version == $hash[$hashkey]->version && */
                    $translation->revision > $hash[$hashkey]->revision) {
                $hash[$hashkey] = $translation;
            }
        } else {
            $hash[$hashkey] = $translation;
        }
    }
    return $hash;
}

/* Get the last revision of that version. Note may be null. */
function getTranslation($dbh, $keyId, $langId, $version) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT * FROM '.$prefix.'translation WHERE id = ? AND lang = ? AND version = ? ORDER BY revision DESC LIMIT 1');
    $stmt->execute(array($keyId, $langId, $version));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Translation');
    return $stmt->fetch();
}
function getTranslationRevision($dbh, $keyId, $langId, $version, $revision) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT * FROM '.$prefix.'translation WHERE id = ? AND lang = ? AND version = ? AND revision = ?');
    $stmt->execute(array($keyId, $langId, $version,$revision));
    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Translation');
    return $stmt->fetch();
}

// Note: for both strings and arrays
function deleteTranslationRevision($dbh, $keyId, $langId, $version, $revision) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('DELETE FROM '.$prefix.'translation WHERE id = ? AND lang = ? AND version = ? AND revision = ?');
    return $stmt->execute(array($keyId, $langId, $version,$revision));
}

function addTranslation($dbh, $editorId, $keyId, $langId, $version, $revision, $text) {
    addArrayTranslation($dbh, $editorId, $keyId, 0, $langId, $version, $revision, $text);
}

function addArrayTranslation($dbh, $editorId, $keyId, $sorder, $langId, $version, $revision, $text) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('INSERT INTO '.$prefix.'translation (editor, id, sorder, lang, version, revision, text) VALUES (?,?,?,?,?,?,?)');
    $stmt->execute(array($editorId, $keyId, $sorder, $langId, $version, $revision, $text));
}

function getTranslationHistory($dbh, $keyId, $langId) {
    $prefix = getDbTablePrefix();
//    SELECT t.*,e.account AS editorname FROM '.$prefix.'translation AS t LEFT JOIN '.$prefix.'editor AS e ON t.editor=e.id 
//    WHERE t.id=917 AND (lang = 7 OR lang = 1) ORDER BY lang DESC, version DESC, revision DESC;

    $stmt = $dbh->prepare('SELECT t.*,e.account AS editorname,l.name AS langname FROM '.$prefix.'translation AS t'
            . ' LEFT JOIN '.$prefix.'editor AS e ON t.editor=e.id'
            . ' LEFT JOIN '.$prefix.'language AS l ON t.lang=l.id'
            . ' WHERE t.id=? AND (t.lang = ? OR t.lang = '.DEFAULT_TRANSLATION_ID.') ORDER BY lang DESC, version DESC, revision DESC');
//    $stmt = $dbh->prepare('SELECT * FROM '.$prefix.'translation WHERE id=? AND (lang = ? OR lang = '.DEFAULT_TRANSLATION_ID.') ORDER BY lang DESC, version DESC, revision DESC');
    $stmt->execute(array($keyId, $langId));
    return $stmt->fetchAll(PDO::FETCH_CLASS, 'Translation');
}

function getTranslationShortStatus($dbh, $langId) {
    return round(100 * getTranslationCompleteness($dbh, $langId), 1).'% complete, ' . 
           round(100 * getTranslationUpdated($dbh, $langId), 1).'% updated' ;
}

// Return a percent number of how complete translation is to a language
function getTranslationCompleteness($dbh, $langId) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT count(DISTINCT t.id) / count(DISTINCT k.id) FROM '.$prefix.'stringkey AS k LEFT JOIN '.$prefix.'translation AS t ON k.id=t.id AND t.lang=? WHERE k.translatable=TRUE AND k.active=TRUE');
    $stmt->execute(array($langId));
    return $stmt->fetchColumn();
}

// Return a percent number of how updated a translation is to a language
function getTranslationUpdated($dbh, $langId) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT count(DISTINCT t.id) / count(DISTINCT k.id) FROM '.$prefix.'stringkey AS k LEFT JOIN '.$prefix.'translation AS t ON k.id=t.id AND t.lang=? AND k.lastversion=t.version WHERE k.translatable=TRUE AND k.active=TRUE');
    $stmt->execute(array($langId));
    return $stmt->fetchColumn();
}

// Note : Always checks latest version on default language
function getTranslationMaxSorder($dbh, $keyId) {
    $prefix = getDbTablePrefix();
    $stmt = $dbh->prepare('SELECT max(t.sorder) FROM '.$prefix.'stringkey AS k LEFT JOIN '.$prefix.'translation AS t ON k.id=t.id AND k.lastversion=t.version WHERE lang = 1 AND k.id=?');
    $stmt->execute(array($keyId));
    return $stmt->fetchColumn();    
}
