<?php

// Display name of you project
function settingProjectName() {
    return 'YourProjectName';
}

// If you want to replace to logo image
// Return null to just use default logo
function settingTopLogoUrl() {
    return null;
    //return 'https://your-server/your-path/your-logo.png';
}

// Connect to the database
function settingDbPdoParams() {
    // Sample database connection, MySQL/MariaDB
    $dsn = 'mysql:dbname=DBNAME;host=localhost';
    $dbuser = 'DBUSER';
    $dbpassword = 'DBPASSWORD';
    return array($dsn, $dbuser, $dbpassword);
}

// Who may do everything?
// You may actually do whatever you need, just return something that may be used as true or false
function settingIsAdmin($username) {
//    $adminUsers = array('admin', ''trusteduser1', 'trusteduser2');
    $adminUsers = array();
    return in_array($username, $adminUsers, TRUE);
}


// Optional settings:

// Database table names prefix (when sharing database)
function settingDbTablesPrefix() {
    //return 'myapp_';
    return '';
}

// If this method does not exist, it defaults to false
function settingAutoCreateAccount() {
    return false;
    //return true;
}

function settingGoogleSignInClientId() {
    return null;
    // If using google login. Something similar to: 373234523423423-p45sample4567456samplekbu5h.apps.googleusercontent.com
    // See: https://developers.google.com/identity/sign-in/web/sign-in#before_you_begin
    // click on CONFIGURE A PROJECT
}

function settingIsAdminGoogleId($googleId) {
    // if ($googleId === "your-google-id") { return true; }
    return false;
}

//function settingGetAccountNameFromGoogleId($googleId) {
    // Custom code to retrieve the name of the person. Normally a player name.
    //return $userName;
//}

function settingCookieName() {
    return "TRANSLATEAPP";
    // If using google login and having several instances on the same server. Set this to different values on the instances. 
    // Set to whatever you want that is a valid cookie name
}
