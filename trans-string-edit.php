<?php 
include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);
$langId = filter_input(INPUT_GET, 'l', FILTER_VALIDATE_INT);
$lang = getLanguage($dbh, $langId);
$keyId = filter_input(INPUT_GET, 'k', FILTER_VALIDATE_INT);
$stringKey = getStringKey($dbh, $keyId);
$nextTodo = filter_input(INPUT_GET, 'next', FILTER_VALIDATE_BOOLEAN);
/* @var $stringKey StringKey  */
htmlHead($lang->name. ' - '.$stringKey->skey, $editor);
htmlBackLink($langId);

$history = getTranslationHistory($dbh, $keyId, $langId);
$latestDefault = null;
$latestTranslation = null;
foreach ($history as $translation) {
    /* @var $translation Translation  */
    if ($translation->lang == DEFAULT_TRANSLATION_ID) {
        if (is_null($latestDefault)) {$latestDefault = $translation;}
    }
    if ($translation->lang == $langId) { // Note: can be default too
        if (is_null($latestTranslation)) {$latestTranslation = $translation;}
    }
}

echo '<form action="trans-string-save.php" method="GET">';

if ($langId != DEFAULT_TRANSLATION_ID || !($editor->admin ?? false)) {
    if ($stringKey->description) {
        echo '<p>'.htmlentities($stringKey->description, ENT_COMPAT | ENT_HTML401, "UTF-8").'<p>';
    }
} else {
    echo 'Key: <input name="stringkey" value="'.htmlentities($stringKey->skey, ENT_COMPAT | ENT_HTML401, "UTF-8").'"><br/>';
    echo 'Description (shown to translators): <textarea name="description">'.htmlentities($stringKey->description, ENT_COMPAT | ENT_HTML401, "UTF-8").'</textarea>';
}

echo '<h2>Current values</h2>';
if ($latestDefault != null && $langId != DEFAULT_TRANSLATION_ID) {
    echo '<div>';
    if ($editor->admin ?? false) {
        echo '<a href="?l='.DEFAULT_TRANSLATION_ID.'&amp;k='.$keyId.'">Default</a>:';
    } else {
        echo 'Default:';
    }
    if ($editor) {
        echo '<button type="button" onclick="document.getElementById(\'enter-text\').innerHTML=document.getElementById(\'default-text\').innerHTML"/>Copy</button>';
    }
    echo '<pre class="current-translation" id="default-text">'.htmlentities($latestDefault->text, ENT_COMPAT | ENT_HTML401, "UTF-8").'</pre></div>';
}
if ($latestTranslation != null) {
    echo '<div>Last translation: '.'<pre class="current-translation">'.htmlentities($latestTranslation->text, ENT_COMPAT | ENT_HTML401, "UTF-8").'</pre></div>';
}

echo '<input type="hidden" name="l" value="'.$langId.'"/>';
echo '<input type="hidden" name="k" value="'.$keyId.'"/>';
echo '<input type="hidden" name="v" value="'.$stringKey->lastversion.'"/>'; 
if ($nextTodo) {
    echo '<input type="hidden" name="next" value="1"/>';
}
if ($editor) {
    echo '<textarea class="enter-translation enter-string-translation" name="text" autofocus id="enter-text">';
    if ($latestTranslation != null && $latestTranslation->version == $stringKey->lastversion) {
        echo htmlentities($latestTranslation->text, ENT_COMPAT | ENT_HTML401, "UTF-8");
    }
    echo '</textarea>';
}
if ($langId == DEFAULT_TRANSLATION_ID) {
    echo '<br>Translatable: <input type="checkbox" name="translatable"'.($stringKey->translatable ? ' checked' : '').'/>';
    if ($editor->admin ?? false) {
        echo '<br>Minor edit (don\'t invalidate translations): <input type="checkbox" name="minor" checked/>';
    }
}
if ($editor) {echo '<br/><input type="submit" value="Submit"/>';}
echo '</form>';

echo '<h2>History:</h2><table><thead><tr><th>language</th><th>time</th><th>version</th><th>revision</th><th>editor</th></tr></thead><tbody>';
foreach ($history as $translation) {
    /* @var $translation Translation  */
    echo '<tr><td>'.join('</td><td>', array($translation->langname, $translation->created, $translation->version, $translation->revision, $translation->editorname)).'</td></tr>';
    if ($translation->lang == $langId && (
            $translation->lang != DEFAULT_TRANSLATION_ID && $translation->editor == ($editor->id ?? null) ||
            ($editor->admin ?? false) && ($translation->lang != DEFAULT_TRANSLATION_ID || $translation->revision > 0))) {
        echo '<tr><td colspan="4"></td><td colspan="2"><a onClick="return confirm(\'Are you sure you want to delete? (can not be undone)\')" href="trans-delete.php?l='.$langId.'&amp;k='.$keyId.'&amp;v='.$translation->version.'&amp;r='.$translation->revision.'">Delete</a></td>';
    }
    echo '<tr><td style="border-bottom:1px solid #777;" colspan="5">'.htmlentities($translation->text, ENT_COMPAT | ENT_HTML401, "UTF-8").'</td></tr>';
}
echo '</tbody></table>';

if ($langId == DEFAULT_TRANSLATION_ID && ($editor->admin ?? false)) {
    echo '<h2>Inactivate</h2>'
    . '<form action="inactive.php" method="GET">'
    . '<input type="hidden" name="k" value="'.$keyId.'">'
    . 'Enter "1" and press button remove key (put in archive): '
    . '<input name="confirm"><input type="submit" value="Inactivate">'
    . '</form>';
}

htmlFoot();
