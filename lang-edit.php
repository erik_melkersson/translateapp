<?php 
include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);
$langId = filter_input(INPUT_GET, 'l', FILTER_VALIDATE_INT);
$lang = getLanguage($dbh, $langId);
htmlHead($lang->name.' ('.$lang->code.')', $editor);
htmlBackLink();

$stringkeys = getStringKeys($dbh);
$translations = getTranslationHash($dbh, $langId);

$groupNew = array();
$groupUpdate = array();
$groupDone = array();
$nextTodo = null;

foreach ($stringkeys as $stringKey) {
    /* @var $stringKey StringKey  */
    if ($langId == DEFAULT_TRANSLATION_ID || $stringKey->translatable) {
        $translationText = "";
        $translation = null;
        /* @var $translation Translation  */
        if (isset($translations[$stringKey->id . '_0'])) {
            $translation = $translations[$stringKey->id . '_0'];
            $translationText = $translation->text;
        }
        if ($stringKey->array) {
            $link = 'trans-array-edit.php?l='.$langId.'&amp;k='.$stringKey->id;
            $item = '<li><a href="'.$link.'">'.$stringKey->skey.'</a>: <strong>string-array</strong>('.htmlentities($translationText, ENT_COMPAT | ENT_HTML401, "UTF-8").', ...)</li>';            
        } else {
            $link = 'trans-string-edit.php?l='.$langId.'&amp;k='.$stringKey->id;
            $item = '<li><a href="'.$link.'">'.$stringKey->skey.'</a>: '.htmlentities($translationText, ENT_COMPAT | ENT_HTML401, "UTF-8").'</li>';
        }
        if ($translation == null) {
            if (is_null($nextTodo)) {
                $nextTodo = $link;
            }
            $groupNew[] = $item;
        } else if ($translation->version < $stringKey->lastversion) {
            $groupUpdate[] = $item;
        } else {
            $groupDone[] = $item;
        }
    }
}

if ($nextTodo) {
    echo '<p><a href="'.$nextTodo.'&amp;next=1"><strong>=&gt; Start translating</strong></a></p>';
}

if ($lang->id != DEFAULT_TRANSLATION_ID || ($editor && $editor->admin)) {
    
    echo '<h2>Working offline?</h2><p><a href="export.php?l='.$lang->id.'">Export</a> <a href="import.php?l='.$lang->id.'">Import</a></p>';
    if (sizeof($groupDone) == 0) {
        echo '<h2>Not used?</h2><p><a href="lang-delete.php?l='.$lang->id.'">Remove language</a></p>';
    }
}

if (sizeof($groupNew) > 0) {echo '<h2>List of new strings</h2><ul>'.join("\n", $groupNew).'</ul>';}
if (sizeof($groupUpdate) > 0) {echo '<h2>Update needed</h2><ul>'.join("\n", $groupUpdate).'</ul>';}
if (sizeof($groupDone) > 0) {echo '<h2>Existing translations</h2><ul>'.join("\n", $groupDone).'</ul>';}

if ($langId == DEFAULT_TRANSLATION_ID && $editor && $editor->admin) { 
    echo '<p><a href="key-add.php">Add more keys</a></p>';
}
htmlBackLink();
htmlFoot();
