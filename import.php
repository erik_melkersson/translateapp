<?php
// Upload a file and let 
include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);

$langId = filter_input(INPUT_GET, 'l', FILTER_VALIDATE_INT);
if (!$langId) {
    // When submitting the file, it uses POST
    $langId = filter_input(INPUT_POST, 'l', FILTER_VALIDATE_INT);
}
$lang = getLanguage($dbh, $langId);
htmlHead('Import '.$lang->code, $editor);
htmlBackLink();

if ($langId != DEFAULT_TRANSLATION_ID || $editor && $editor->admin) {

if (isset($_FILES['file']) && ($_FILES['file']['error'] == UPLOAD_ERR_OK)) {
    $xml = simplexml_load_file($_FILES['file']['tmp_name']);

    $stringKeyHash = getStringKeyHash($dbh);
    $translations = getTranslationHash($dbh, $langId);
    $defaultTranslations = getTranslationHash($dbh, DEFAULT_TRANSLATION_ID);

    $stringCount = 0;
    $arrayCount = 0;
    foreach ($xml->children() as $string) {
        $translatable = ($string['translatable']) ?? null != "false";
        $stringKey = $stringKeyHash[(string)$string['name']] ?? null;
        if ($string->getName() == "string") {
            $translation = null;
            if (isset($translations[($stringKey->id ?? null) . '_0'])) {
                $translation = $translations[$stringKey->id . '_0'];
            }

            $text = decodeString((string)$string);

            if ($langId == DEFAULT_TRANSLATION_ID) {
                if (is_null($stringKey)) {
                    echo "NEW KEY: ".$string['name'].': '.$text.' '.$translatable."<br>";
                    $keyId = addStringKey($dbh, $string['name'], $translatable, false, '');
                    addTranslation($dbh, $editor->id, $keyId, $langId, 0, 0, $text);
                } else if (is_null($translation)) {
                    echo "NEW TRANSLATION: ".$string['name'].': '.$text.' '.$translatable."<br>";
                    addTranslation($dbh, $editor->id, $stringKey->id, $langId, 0, 0, $text);
                } else if ($translation->text != $text) {
                    echo "UPDATED: ".$string['name'].': '.$text."<br>";
                    $version = updateLastVersion($dbh, $stringKey->id);
                    addTranslation($dbh, $editor->id, $stringKey->id, $langId, $version, 0, $text);
                } else {
                    //echo "SAME, IGNORING: ".$string['name'].': '.(string)$string."<br>";
                }
            } else {
                if (is_null($stringKey)) {
                    echo("WARNING IGNORING: No such key:" . $string['name'].'<br>');
                } else {
                    $default = null;
                    if (isset($defaultTranslations[$stringKey->id . '_0'])) {
                        $default = $defaultTranslations[$stringKey->id . '_0'];
                    }
                    if (is_null($translation)) {
                        if (is_null($default) || $default->text != $text) {// must differ from default to count.
                            echo "NEW TRANSLATION: ".$string['name'].': '.$text."<br>";
                            addTranslation($dbh, $editor->id, $stringKey->id, $langId, $stringKey->lastversion, 0, $text); // TODO Using lastversion, don't know if good or bad...
                        } else {
                            //echo "SAME AS DEFAULT, IGNORING: ".$string['name'].': '.(string)$string."<br>";
                        }
                    } else if ($translation->text != $text) {
                        echo "UPDATED: ".$string['name'].': '.$text."<br>";
                        $revision = 0;
                        if ($translation->version == $stringKey->lastversion) { $revision = $translation->revision + 1; }
                        addTranslation($dbh, $editor->id, $stringKey->id, $langId, $stringKey->lastversion, $revision, $text);
                    } else {
                        //echo "SAME, IGNORING: ".$string['name'].': '.(string)$string."<br>";
                    }
                }
            }           
            $stringCount++;
        } else if ($string->getName() == "string-array") {
            if ($langId == DEFAULT_TRANSLATION_ID) {
                // Get the item values from the xml
                $itemString = array();
                foreach ($string->children() as $item) {
                    $itemString[] = decodeString((string)$item);
                }
                // If new
                if (is_null($stringKey)) {
                    $keyId = addStringKey($dbh, $string['name'], $translatable, true, '');
                    for($sorder = 0; $sorder < sizeof($itemString); $sorder++) {
                        addArrayTranslation($dbh, $editor->id, $keyId, $sorder, $langId, 0, 0, $itemString[$sorder]);
                    }
                    echo "NEW ARRAY INSERTED !".$string['name'].'<br>';
                } else {
                    // Compare to current values
                    $equal = true;
                    for($sorder = 0; $sorder < sizeof($itemString); $sorder++) {
                        if (!isset($defaultTranslations[$stringKey->id.'_'.$sorder]) || 
                                $defaultTranslations[$stringKey->id.'_'.$sorder]->text != $itemString[$sorder]) {
                            $equal = false; break;
                        }
                    }
                    if (!$equal) {
                        echo 'UPDATED ARRAY: '.$string['name'].'<br>';
                        $version = updateLastVersion($dbh, $stringKey->id);
                        for($sorder = 0; $sorder < sizeof($itemString); $sorder++) {
                            addArrayTranslation($dbh, $editor->id, $stringKey->id, $sorder, $langId, $version, 0, $itemString[$sorder]);
                        }
                    } else {
                        echo 'ARRAY THE SAME: '.$string['name'].'<br>';
                    }
                }
            } else {
                echo "NOT YET SUPPORTED: string-array in other than default. Please conribute and add code for this in import.";
            }
            $arrayCount++;
        } else {
            echo "NOT SUPPORTED. SOMETHING ELSE FOUND:".$string->getName().'<br>';
        }
    }
    echo "Found and checked $stringCount strings and $arrayCount arrays.<br>";

    // Info about the parser
    // https://www.w3schools.com/php/php_xml_simplexml_get.asp
} else {

?>
<form action="import.php" method="post" enctype="multipart/form-data">
  <input type="file" name="file" id="file" />
  <input type="hidden" name="l" value="<?php echo htmlspecialchars($langId);?>" />
  <input type="submit" value="Submit" />
</form>

<?php
} // end of else on file submitted
} else {
    echo '<p>Only admin may upload default file</p>';
}
htmlFoot();

function decodeString($string) {
    if (substr($string, -1) == '"' && substr($string, 0, 1) == '"') {
        $string = substr($string, 1, strlen($string)-2);
        // new lines are ok
        // ' should be ok too
    } else {
        $string = 
            str_replace('\\n', "\n",  // \n -> new line
            str_replace('\\\'', "'",  // \' -> '
                $string));
    }
    return
        str_replace('\\"', '"',  // \" -> "
        str_replace('\\\\', '\\',  //  \\ -> \
            $string));

    // NOTE: Don't touch % and %%
}
