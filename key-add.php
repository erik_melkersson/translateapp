<?php 
include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);
htmlHead("Add string key", $editor);

$key = filter_input(INPUT_GET, 'key', FILTER_SANITIZE_STRING);
$translatable = filter_input(INPUT_GET, 'translatable', FILTER_VALIDATE_BOOLEAN);
if (!$translatable) { $translatable = false; }
$array = filter_input(INPUT_GET, 'array', FILTER_VALIDATE_BOOLEAN);
if (is_null($array)) { $array = FALSE; }
$text = filter_input(INPUT_GET, 'text', FILTER_UNSAFE_RAW);
$description = filter_input(INPUT_GET, 'description', FILTER_UNSAFE_RAW);

if ($editor && $editor->admin) {
if ($key) {
    $keyId = addStringKey($dbh, $key, $translatable, $array, $description);
    addTranslation($dbh, $editor->id, $keyId, DEFAULT_TRANSLATION_ID, 0, 0, $text); // Fixed lang as it is the default text, fixed version as it is the first
    echo '<p>'.$key.' added<p>';
} else {
    echo '<form action="key-add.php" method="GET">Enter string key: <input name="key" autofocus/><br>'
    . 'Description (shown to translators): <input name="description"/><br>'
    . 'Translatable: <input type="checkbox" name="translatable" checked/><br>'
    . 'String-Array: <input type="checkbox" name="array"/><br>'
            . 'Default text: <input name="text"/> (or first string in array)<br>'
            . '<input type="submit" value="Submit"/></form>';
}
} else {
    echo '<p>Only admin may add new keys</p>';
}
htmlBackLink(DEFAULT_TRANSLATION_ID);
htmlFoot();
