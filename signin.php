<?php
require_once 'include/common.php';
require_once __DIR__.'/vendor/autoload.php';

$dbh = getDbh();

$idToken = $_POST['credential'] ?? null;
$returnTo = $_POST['returnurl'];

// Check that idtoken is ok
$googleAccountPayload = getGoogleAccount($idToken);
if ($googleAccountPayload === false) {
    htmlHead("Log in failed", true);
    echo 'Invalid login.';
    htmlFoot();
} else {
    // If it should auto-create new accounts
    $autoCreate = function_exists('settingAutoCreateAccount') && settingAutoCreateAccount();
    
    // Find or create account and set a new session
    $session = getEditorSessionByGoogleId($dbh, $googleAccountPayload['sub'], $googleAccountPayload['name'], $googleAccountPayload['email'], $autoCreate);
    if ($session == null) {
        htmlHead("No such account", true);
        echo 'Follow the instructions to create an account first. <a href="#" onclick="signOut();">Back</a>.';
        htmlFoot();        
    } else {
        // Return cookie with session
        setcookie(getSessionCookieName(), $session);
        // Return redirect to list.php
        if ($returnTo) {
            // TODO check that relative path inside current server
            header("Location: " . $returnTo);
        } else {
            header("Location: index.php");
        }
    }
}   

function getGoogleAccount($idToken) {
    $clientId = settingGoogleSignInClientId();
    $client = new Google_Client(['client_id' => $clientId]);
    $payload = $client->verifyIdToken($idToken);
    if (!$payload) {
        // Invalid ID token
        return false;
    } 
    return $payload; // google user id is in  ['sub'], where is the email?;
}
    
?>
