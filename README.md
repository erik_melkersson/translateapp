# Features

* Multiple users can edit. Use your own login system, from your game or whatever.
* Two levels of users: Admin and logged in. Only admin can add keys/import default language file/toggle if a key is translatable
* Version management of translations.
* Handles string and string-array. It does not handle plural-variants.
* Import/export
* Edit on site
* Build in support for google account login (if you want to use it)
* Looks really bad :-)
* Minor updates of original language, not invalidating translations as normal updates do.
* And more...

# Requirements

* A web server with php
* A mysql-database with create and write access

Most web hosting companies offer this.

# Setup

* Clone this repo somewhere.
* If you want to use the built in google sign in support you have to add google-api-php-client either using Composer or share the folder on the server
** <code>composer install</code>
** OR
** Symlink /vendor/ to existing folder on server to share the folder.
* Move/rename the directory to where you want it. (Preferably somewhere under the web root directory)
* Enable PHP on the directory in the web server (if not already enabled)
* If you use built in google sign in you enable it in the settings.php (read below)
* If you want to use custom sign in you setup some kind of auth in the web server (Apache). 
** Example basic auth. Note: The auth method MUST set PHP_SERVER_AUTH
** copy .htaccess-sample to .htaccess. Edit the name and the path.
** Create a password file with for example "htpasswd -c .htpasswd YOURUSERNAME"
* Make sure the include subdirectory is not accessable. Try by browsing to //yoursite/installationdir/include/index.html
* Create a database and a database user with create and write access to that database
* Copy include/settings_template.php to include/settings.php and 
** set database connection values 
** enable/disable google login methods if you want to use them. (see the settings_template.php)
* Load index.php in a browser and start using it (it should setup tables in the database by itself if needed)

## Setup a db in mysql
 * CREATE DATABASE yourdbname;
 * CREATE USER 'yourdbusername'@'localhost' IDENTIFIED BY 'yourdbpassword';
 * GRANT ALL PRIVILEGES ON yourdbname.* TO 'yourdbusername'@'localhost';
 * FLUSH PRIVILEGES;

Optional:
*  Enter extra styling in style/style.css

# Using it

## Encoding/decoding
It decodes and encodes ', ", row breaks/\n, \ but do not touch % and %%.
This means that the translators only have to deal with the % placeholders and escaping %-sigs manually (%%) when wanted.

## Administration
Editors may remove the translations they have added. (If they made something wrong, they should be allowed to fix it) Admin may remove all.

## Updates
* 2018-03-09 Admins may invalidate translations keys, when no longer needed.
* 2019-02-09 Built in support for google sign in.
* 2020-03-26 Limit string to 16k charactes as new db-version only handle that when using utf8
* 2020-03-29 Added a test so admin may test if placeholders in translated strings matches the original string
* 2021-08-07 Support for having a prefix on the table names so they may share database with other things.

# Developed by
[Lingonberry Games](https://lingonberry.games/) Mostly making games for Android.