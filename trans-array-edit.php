<?php 
include 'include/common.php';
$dbh = getDbh();
$editor = getEditor($dbh);
$langId = filter_input(INPUT_GET, 'l', FILTER_VALIDATE_INT);
$lang = getLanguage($dbh, $langId);
$keyId = filter_input(INPUT_GET, 'k', FILTER_VALIDATE_INT);
$stringKey = getStringKey($dbh, $keyId);
/* @var $stringKey StringKey  */
htmlHead($lang->name. ' - '.$stringKey->skey, $editor);

$history = getTranslationHistory($dbh, $keyId, $langId);
$maxSorder = getTranslationMaxSorder($dbh, $keyId);

$translations = getTranslationHash($dbh, $langId);
$defaultTranslations = getTranslationHash($dbh, DEFAULT_TRANSLATION_ID);

if ($editor) {
    echo '<form action="trans-array-save.php" method="GET">';

    if ($langId != DEFAULT_TRANSLATION_ID || !($editor->admin ?? false)) {
        if ($stringKey->description) {
            echo '<p>'.htmlentities($stringKey->description, ENT_COMPAT | ENT_HTML401, "UTF-8").'<p>';
        }
    } else {
        echo 'Key: <input name="stringkey" value="'.htmlentities($stringKey->skey, ENT_COMPAT | ENT_HTML401, "UTF-8").'"><br/>';
        echo 'Description (shown to translators): <textarea name="description">'.htmlentities($stringKey->description, ENT_COMPAT | ENT_HTML401, "UTF-8").'</textarea>';
    }

    echo '<h2>Update</h2>';
    echo '<input type="hidden" name="l" value="'.$langId.'"/>';
    echo '<input type="hidden" name="k" value="'.$keyId.'"/>';
    echo '<input type="hidden" name="v" value="'.$stringKey->lastversion.'"/>'; 
    
    for ($sorder = 0; $sorder <= $maxSorder; $sorder++) {
        echo htmlentities($defaultTranslations[$keyId.'_'.$sorder]->text, ENT_COMPAT | ENT_HTML401, "UTF-8").'<br>';
        $value = isset($translations[$keyId.'_'.$sorder]) ? $translations[$keyId.'_'.$sorder]->text : '';
        echo '<input style="width: 90%;" name="text'.$sorder.'" value="'.htmlentities($value, ENT_COMPAT | ENT_HTML401, "UTF-8").'" /><br>';    
    }
    if ($langId == DEFAULT_TRANSLATION_ID) {
        echo '<br>+ Add more values if needed:';
        for ($sorder = $maxSorder + 1; $sorder <= ($maxSorder > 4 ? $maxSorder + 2 : 6); $sorder++) {
            echo '<input style="width: 90%;" name="text'.$sorder.'" /><br>';    
        }
    
        echo '<br>Translatable: <input type="checkbox" name="translatable"'.($stringKey->translatable ? ' checked' : '').'/>';
        //if ($editor->admin ?? false) {
            echo '<br>Minor edit (don\'t invalidate translations): <input type="checkbox" name="minor" checked />';
        //}
    }
    echo '<br/><input type="submit" value="Submit"/>';
    echo '</form>';
}

echo '<h2>History:</h2><table><thead><tr><th>language</th><th>time</th><th>version</th><th>revision</th><th>editor</th></tr></thead><tbody>';
$lastT = null;
/* @var $lastT Translation  */
foreach ($history as $translation) {
    /* @var $translation Translation  */
    if (is_null($lastT) || $lastT->lang != $translation->lang || $lastT->version != $translation->version || $lastT->revision != $translation->revision) {
        echo '<tr><td style="border-top:1px solid #777;">'.join('</td><td style="border-top:1px solid #777;">', array($translation->langname, $translation->created, $translation->version, $translation->revision, $translation->editorname)).'</td></tr>';
        if ($translation->lang == $langId && (
            $translation->lang != DEFAULT_TRANSLATION_ID && $translation->editor == ($editor->id ?? null) ||
            ($editor->admin ?? false) && ($translation->lang != DEFAULT_TRANSLATION_ID || $translation->revision > 0))) {
        echo '<tr><td colspan="4"></td><td colspan="12"><a onClick="return confirm(\'Are you sure you want to delete? (can not be undone)\')" href="trans-delete.php?l='.$langId.'&amp;k='.$keyId.'&amp;v='.$translation->version.'&amp;r='.$translation->revision.'">Delete</a></td>';
    }

    }
    echo '<tr><td colspan="5">'.htmlentities($translation->text, ENT_COMPAT | ENT_HTML401, "UTF-8").'</td></tr>';
    $lastT = $translation;
}
echo '</tbody></table>';

htmlBackLink();
htmlFoot();
