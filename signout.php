<?php
require_once 'include/common.php';

$dbh = getDbh();

$session = filter_input(INPUT_COOKIE, getSessionCookieName(), FILTER_UNSAFE_RAW);
$returnTo = filter_input(INPUT_GET, "ret", FILTER_UNSAFE_RAW);

if ($session) {
    deleteSession($dbh, $session);
    setcookie(getSessionCookieName(), '', time()-3600);
}

if ($returnTo) {
    // TODO check that relative path inside current server
    header("Location: " . $returnTo);
} else {
    header("Location: index.php");
}

    
?>
